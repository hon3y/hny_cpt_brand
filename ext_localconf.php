<?php
defined('TYPO3_MODE') || die('Access denied.');
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
/*
 * See: https://usetypo3.com/icon-api.html
 * See: https://sebastiaandejonge.com/blog/2016/october/19/adding-custom-content-elements-to-the-content-element-wizard-with-typo3-cms.html
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Imaging\IconRegistry::class
);

// SVGs
$iconRegistry->registerIcon(
    "hive_cpt_brand_16x16_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_24x24_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_24x24.svg']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_32x32_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_32x32.svg']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_64x64_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_64x64.svg']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_128x128_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_128x128.svg']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_256x256_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_256x256.svg']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_512x512_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_512x512.svg']
);
// TYPO3 Plugin Icon - HIVE Style
$iconRegistry->registerIcon(
    "plugins_cpt_brand_32x32_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/plugins_32x32.svg']
);
// Bootstrap Icon - HIVE Style
$iconRegistry->registerIcon(
    "bootstrap_cpt_brand_32x32_svg",
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/bootstrap_32x32.svg']
);

// PNGs
$iconRegistry->registerIcon(
    "hive_cpt_brand_16x16_png",
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.png']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_24x24_png",
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_24x24.png']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_32x32_png",
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_32x32.png']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_64x64_png",
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_64x64.png']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_128x128_png",
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_128x128.png']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_256x256_png",
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_256x256.png']
);
$iconRegistry->registerIcon(
    "hive_cpt_brand_512x512_png",
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_512x512.png']
);

//call_user_func(
//    function($extKey)
//    {
//
//        // wizards
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
//            'mod {
//                wizards.newContentElement.wizardItems {
//                    hive {
//                        header = Hive
//                        after = common,special,menu,plugins,forms
//                        elements.helloWorld {
//                            iconIdentifier = hive_cpt_brand_32x32_svg
//                            title = Hello World
//                            description = Hello world element
//                            tt_content_defValues {
//                                CType = text
//                                bodytext (
//                                    <h2>Hello World</h2>
//                                    <p class="bodytext">Lorem ipsum dolor sit amet, consectetur, sadipisci velit ...</p>
//                                )
//                                header = Section Header
//                                header_layout = 100
//                            }
//                        }
//                        show := addToList(helloWorld)
//                    }
//
//                }
//            }'
//        );
//
//    }, $_EXTKEY
//);

/*
 * Backend styles
 */
$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = serialize(
    [
        'loginLogo'  => 'EXT:hive_cpt_brand/Resources/Public/Icons/PNG/hive_128x128.png',
        'loginHighlightColor'  => '#009982',
        'loginBackgroundImage' => '',
        'backendFavicon' => 'EXT:hive_cpt_brand/Resources/Public/Icons/hive_favicon.ico',
        'backendLogo' => 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_24x24.svg'
    ]
);



